import React, {Component} from 'react'
import {StyleSheet, Text, View, Image, TouchOpacity } from 'react-native';
import Swiper from 'react-native-animated-swiper';

class App extends Component {

//const Example = () => (
render () {
      return (
      <View style = {styles.container}>
      //Swiper is the caller
          <Swiper
            dots
            dotsColor="rgba(97, 218, 251, 0.25)"
            dotsColorActive="rgba(97, 218, 251, 1)"
            dotsBottom
            style={styles.slides}>

            <Image source={require('./img/J1.jpg')} style={styles.img} />
            <TouchOpacity>
            <Image source={require('./img/p.png')} style={styles.img} />
            </TouchOpacity>
            <Image source={require('./img/p.png')} style={styles.img} />
            <Image source={require('./img/p.png')} style={styles.img} />
            <Image source={require('./img/p.png')} style={styles.img} />
          </Swiper>

          <Text style={styles.text}>Jorell Socorro</Text>
          <Text style={styles.text2}>I go to Illinois Tech and part of Omega Delta</Text>

      </View>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    marginTop:20,
    justifyContent: 'center',
    alignItems: 'center',
    height: 300
     },
  img: {
    justifyContent:'center',
    width: 320,
    height: 300,
    flex:1
  },
  text: {
    marginTop:10
  }

});

export default App;
