import React from 'react';
import { StyleSheet, Text, View, Image, Button, TouchableHighlight, CameraRoll} from 'react-native';
import Swiper from 'react-native-animated-swiper';
import { ImagePicker, Permissions } from 'expo';


export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      test: false,
      image_pic: null,
      hasCameraPermission: false,
      hasGalleryPermission: false,
      generalInfo: null,
      error: ''
    };
  }



  /*
  foo () {
    this.setState({test:true});
  }

*/


  render() {
    return (
      <View style={styles.container}>
      <Swiper
        dots
        dotsColor="white"
        dotsColorActive="black"
        dotsBottom={0}
        style={styles.slides}>

        <Image source={require('./img/J1.jpg')} style={styles.img} />
        <TouchableHighlight
           onPress={() => this.goToGallery.bind(this)}>
          <Image
              source={require('./img/p.png')} style={styles.img2} />
        </TouchableHighlight>
      </Swiper>
      </View>
    );
  }
}







const styles = StyleSheet.create({
  container: {
    marginTop:20,
    justifyContent: 'center',
    alignItems: 'center',
    height: 300
     },
  img: {
    justifyContent:'center',
    width: 320,
    height: 300,
    flex:1
  },
  img2:{
    justifyContent:'center',
    alignItems: 'center',
    width: 200,
    height: 200,

  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    alignItems: 'flex-end'

  },
  text: {
    marginTop:10
  }

});
